workspace "Neutrino"
	architecture "x64"
	startproject "Sandbox"

	configurations
	{
		"Debug",
		"Release",
		"Dist"
	}

outputdir = "%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}"

IncludeDir = {}
IncludeDir["GLFW"] = "Neutrino/vendor/GLFW/include"
IncludeDir["Glad"] = "Neutrino/vendor/Glad/include"
IncludeDir["glm"] = "Neutrino/vendor/glm"
IncludeDir["stb_image"] = "Neutrino/vendor/stb_image"

include "Neutrino/vendor/GLFW"
include "Neutrino/vendor/Glad"

project "Neutrino"
	location "Neutrino"
	kind "StaticLib"
	language "C++"
	cppdialect "C++17"
	staticruntime "on"

	targetdir ("bin/" .. outputdir .. "/%{prj.name}")
	objdir ("bin-int/" .. outputdir .. "/%{prj.name}")

	pchheader "ntpch.h"
	pchsource "Neutrino/src/ntpch.cpp"

	files
	{
		"%{prj.name}/src/**.h",
		"%{prj.name}/src/**.cpp",
		"%{prj.name}/vendor/stb_image/**.h",
		"%{prj.name}/vendor/stb_image/**.cpp",
		"%{prj.name}/vendor/glm/glm/**.hpp",
		"%{prj.name}/vendor/glm/glm/**.inl"
	}

	defines
	{
		"_CRT_SECURE_NO_WARNINGS"
	}

	includedirs
	{
		"%{prj.name}/src",
		"%{prj.name}/vendor/spdlog/include",
		"%{IncludeDir.GLFW}",
		"%{IncludeDir.Glad}",
		"%{IncludeDir.stb_image}",
		"%{IncludeDir.glm}"
	}
	links
	{
		"GLFW",
		"Glad",
		"opengl32.lib"
	}

	filter "system:windows"
		systemversion "latest"

		defines
		{
			"NT_PLATFORM_WINDOWS",
			"NT_BUILD_DLL",
			"GLFW_INCLUDE_NONE"
		}

	filter "configurations:Debug"
		defines "NT_DEBUG"
		runtime "Debug"
		symbols "on"

	filter "configurations:Release"
		defines "NT_RELEASE"
		runtime "Release"
		optimize "on"

	filter "configurations:Dist"
		defines "NT_DIST"
		runtime "Release"
		optimize "on"

project "Sandbox"
	location "Sandbox"
	kind "ConsoleApp"
	language "C++"
	cppdialect "C++17"
	staticruntime "on"

	targetdir ("bin/" .. outputdir .. "/%{prj.name}")
	objdir ("bin-int/" .. outputdir .. "/%{prj.name}")

	files
	{
		"%{prj.name}/src/**.h",
		"%{prj.name}/src/**.cpp"
	}

	includedirs
	{
		"Neutrino/vendor/spdlog/include",
		"Neutrino/src",
		"Neutrino/vendor",
		"%{IncludeDir.glm}"
	}

	links
	{
		"Neutrino"
	}

	filter "system:windows"
		systemversion "latest"

		defines
		{
			"NT_PLATFORM_WINDOWS"
		}

	filter "configurations:Debug"
		defines "NT_DEBUG"
		runtime "Debug"
		symbols "on"

	filter "configurations:Release"
		defines "NT_RELEASE"
		runtime "Release"
		optimize "on"

	filter "configurations:Dist"
		defines "NT_DIST"
		runtime "Release"
		optimize "on"