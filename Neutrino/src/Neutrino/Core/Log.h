#pragma once

#include "Core.h"
#include "spdlog/spdlog.h"
#include "spdlog/sinks/stdout_color_sinks.h"
#include "spdlog/fmt/ostr.h"

namespace NT {
	class NEUTRINO_API Log
	{
	public:
		static void Init();
		inline static std::shared_ptr<spdlog::logger>& GetCoreLogger() { return s_CoreLogger; }
		inline static std::shared_ptr<spdlog::logger>& GetClientLogger() { return s_ClientLogger; }
	private:
		static std::shared_ptr<spdlog::logger> s_CoreLogger;
		static std::shared_ptr<spdlog::logger> s_ClientLogger;

	};

}

//core log macros

#define NT_CORE_TRACE(...)		::NT::Log::GetCoreLogger()->trace(__VA_ARGS__)
#define NT_CORE_INFO(...)       ::NT::Log::GetCoreLogger()->info(__VA_ARGS__)
#define NT_CORE_WARN(...)		::NT::Log::GetCoreLogger()->warn(__VA_ARGS__)
#define NT_CORE_ERROR(...)		::NT::Log::GetCoreLogger()->error(__VA_ARGS__)
#define NT_CORE_FATAL(...)		::NT::Log::GetCoreLogger()->fatal(__VA_ARGS__)

//client  log macros

#define NT_TRACE(...)			::NT::Log::GetClientLogger()->trace(__VA_ARGS__)
#define NT_INFO(...)			::NT::Log::GetClientLogger()->info(__VA_ARGS__)
#define NT_WARN(...)			::NT::Log::GetClientLogger()->warn(__VA_ARGS__)
#define NT_ERROR(...)			::NT::Log::GetClientLogger()->error(__VA_ARGS__)
#define NT_FATAL(...)			::NT::Log::GetClientLogger()->fatal(__VA_ARGS__)