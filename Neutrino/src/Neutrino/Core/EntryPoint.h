#pragma once

#include "Log.h"

#ifdef NT_PLATFORM_WINDOWS
namespace NT {
	extern NT::Application* CreateApplication();
}
	int main(int argc, char** argv)
	{
		NT::Log::Init();
		NT_CORE_WARN("Initialized Log");

		auto app = NT::CreateApplication();
		app->Run();
		delete app;
	}
#endif