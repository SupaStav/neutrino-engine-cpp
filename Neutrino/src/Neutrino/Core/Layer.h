#pragma once

#include "Neutrino/Core/Core.h"
#include "Neutrino/Events/Event.h"

#include "Neutrino/Core/Timestep.h"

namespace NT {

	class NEUTRINO_API Layer
	{
	public:
		Layer(const std::string& name = "Layer");
		virtual ~Layer();

		virtual void OnAttach() {}
		virtual void OnDetach() {}
		virtual void OnUpdate(Timestep ts) {}
		virtual void OnImGuiRender() {}
		virtual void OnEvent(Event& event) {}

		inline const std::string& GetName() const { return m_DebugName; }
	protected:
		std::string m_DebugName;
	};

}