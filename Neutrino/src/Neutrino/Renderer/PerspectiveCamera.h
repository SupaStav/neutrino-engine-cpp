#pragma once

#include "Neutrino/Core/Log.h"

#include "Neutrino/Math/Transform.h"

#include <glm/glm.hpp>



namespace NT
{
	class PerspecCamera
	{
	public:
		PerspecCamera(float fov, float aspectRatio, float closeRange, float farRange);
		void SetProjection(float fov, float aspectRatio, float closeRange, float farRange);

		//const glm::vec3& GetPosition() const { return m_Position; }
		//void SetPosition(const glm::vec3& position) { m_Position = position; RecalViewMatrix(); }

		NT::Transform& GetTransform() { return m_Transform; }

		const glm::mat4& GetProjectionMatrix() const { return m_ProjectionMatrix; }
		const glm::mat4& GetViewMatrix() { RecalViewMatrix(); return m_ViewMatrix; }
		const glm::mat4& GetViewProjectionMatrix() { RecalViewMatrix(); return m_ViewProjectionMatrix; }

	private:
		void RecalViewMatrix();
	private:
		glm::mat4 m_ProjectionMatrix;
		glm::mat4 m_ViewMatrix;
		glm::mat4 m_ViewProjectionMatrix;

		NT::Transform m_Transform;
	};
}