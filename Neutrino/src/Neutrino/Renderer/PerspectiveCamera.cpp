#include "ntpch.h"
#include "PerspectiveCamera.h"

#include <cmath>

#include <glm/gtc/matrix_transform.hpp>

namespace NT
{
	PerspecCamera::PerspecCamera(float fov, float aspectRatio, float closeRange, float farRange)
		: m_ProjectionMatrix(glm::perspective(fov, aspectRatio, closeRange, farRange)), m_ViewMatrix(0.0f)
	{
		m_ViewProjectionMatrix = m_ProjectionMatrix * m_ViewMatrix;
	}

	void PerspecCamera::SetProjection(float fov, float aspectRatio, float closeRange, float farRange)
	{
		m_ProjectionMatrix = glm::perspective(fov, aspectRatio, closeRange, farRange);
		m_ViewProjectionMatrix = m_ProjectionMatrix * m_ViewMatrix;
	}

	void PerspecCamera::RecalViewMatrix()
	{
		glm::mat4 transform = m_Transform.GetMatrix();

		m_ViewMatrix = glm::inverse(transform);
		m_ViewProjectionMatrix = m_ProjectionMatrix * m_ViewMatrix;
	}
}