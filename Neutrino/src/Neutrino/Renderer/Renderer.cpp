#include "ntpch.h"
#include "Renderer.h"

#include "Platform/OpenGL/OpenGLShader.h"

namespace NT
{

	Renderer::SceneData* Renderer::m_sceneData = new Renderer::SceneData;

	void Renderer::Init()
	{
		RenderCommand::Init();
	}

	void Renderer::BeginScene(PerspecCamera& camera)
	{
		m_sceneData->ViewProjectionMatrix = camera.GetViewProjectionMatrix();
	}

	void Renderer::EndScene()
	{
	}

	void Renderer::Submit(const Ref<Shader>& shader, const Ref<VertexArray>& vertexArray, const glm::mat4& transform)
	{
		shader->Bind();
		std::dynamic_pointer_cast<OpenGLShader> (shader)->UploadUniformMat4("u_ViewProjection", m_sceneData->ViewProjectionMatrix);
		std::dynamic_pointer_cast<OpenGLShader> (shader)->UploadUniformMat4("u_Transform", transform);

		//materialInstance.Bind();

		vertexArray->Bind();
		RenderCommand::DrawIndexed(vertexArray);
	}
}