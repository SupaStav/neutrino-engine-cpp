#pragma once

#define NT_JOYSTICK_1             0
#define NT_JOYSTICK_2             1
#define NT_JOYSTICK_3             2
#define NT_JOYSTICK_4             3
#define NT_JOYSTICK_5             4
#define NT_JOYSTICK_6             5
#define NT_JOYSTICK_7             6
#define NT_JOYSTICK_8             7
#define NT_JOYSTICK_9             8
#define NT_JOYSTICK_10            9
#define NT_JOYSTICK_11            10
#define NT_JOYSTICK_12            11
#define NT_JOYSTICK_13            12
#define NT_JOYSTICK_14            13
#define NT_JOYSTICK_15            14
#define NT_JOYSTICK_16            15
#define NT_JOYSTICK_LAST          NT_JOYSTICK_16