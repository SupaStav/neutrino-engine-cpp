#pragma once

#include "GamepadCodes.h"
#include "GamepadMiscCodes.h"
#include "JoystickCodes.h"
#include "MouseButtonCodes.h"
#include "KeyCodes.h"