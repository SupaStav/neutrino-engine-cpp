#pragma once

#include "Neutrino/Renderer/Renderer.h"

namespace NT
{
	class Entity
	{
	public:
		void SetIndexBuffer(uint32_t* indexBuffer, unsigned long long size);
		void SetVertexBuffer(float* vertexBuffer, unsigned long long size);
		void SetLayout(NT::BufferLayout& layout);
		void SetTexture(std::string& path);
		void GenVertexArray();
		void SetVertexArray();

		Ref<NT::VertexArray> GetVertexArray() { return m_VertexArray; }

	private:
		Ref<NT::VertexBuffer> m_VertexBuffer;
		Ref<NT::IndexBuffer> m_IndexBuffer;
		Ref<NT::VertexArray> m_VertexArray;
		Ref<NT::BufferLayout> m_BufferLayout;

		bool changed = false;
	};
}

