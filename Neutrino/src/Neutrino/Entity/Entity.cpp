#include "ntpch.h"
#include "Entity.h"

namespace NT
{
	void Entity::SetIndexBuffer(uint32_t* indexBuffer, unsigned long long size)
	{
		m_IndexBuffer.reset(NT::IndexBuffer::Create(indexBuffer, size / sizeof(uint32_t)));
	}
	void Entity::SetVertexBuffer(float* vertexBuffer, unsigned long long size)
	{
		m_VertexBuffer.reset(NT::VertexBuffer::Create(vertexBuffer, size));
	}
	void Entity::SetLayout(NT::BufferLayout& layout)
	{
		m_VertexBuffer->SetLayout(layout);
	}
	void Entity::SetTexture(std::string& path)
	{
	}
	void Entity::GenVertexArray()
	{
		if (changed == true)
			NT_ASSERT(false, "YOU MUST GENERATE A VERTEX ARRAY BEFORE SETTING IT!");
		m_VertexArray.reset(NT::VertexArray::Create());
	}
	void Entity::SetVertexArray()
	{
		changed = true;
		m_VertexArray->AddVertexBuffer(m_VertexBuffer);
		m_VertexArray->SetIndexBuffer(m_IndexBuffer);
	}

}