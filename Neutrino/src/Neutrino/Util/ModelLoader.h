#pragma once

#include <string>

namespace NT
{
	class ModelLoader
	{
		float GetVertexBufferFromModel(const std::string& path);
		uint32_t GetIndexBufferFromFile(const std::string& path);
	};
}