#include "ntpch.h"

#include "OpenGLContext.h"

#include <GLFW/glfw3.h>

#include <glad/glad.h>


namespace NT
{
	OpenGLContext::OpenGLContext(GLFWwindow* windowHandle)
		: m_WindowHandle(windowHandle)
	{
		NT_CORE_ASSERT(windowHandle, "Window handle is NULL!")
	}

	void OpenGLContext::Init()
	{
		glfwMakeContextCurrent(m_WindowHandle);
		int status = gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);
		NT_CORE_ASSERT(status, "Failed to init GLAD");

		NT_CORE_WARN("Vendor : {0}", glGetString(GL_VENDOR));
		NT_CORE_WARN("Renderer : {0}", glGetString(GL_RENDERER));
		NT_CORE_WARN("Version : {0}", glGetString(GL_VERSION));

		//hack
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
	}

	void OpenGLContext::SwapBuffers()
	{
		glfwSwapBuffers(m_WindowHandle);
	}
}