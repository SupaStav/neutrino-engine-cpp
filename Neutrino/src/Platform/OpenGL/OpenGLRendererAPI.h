#pragma once

#include "Neutrino/Renderer/RendererAPI.h"

#include <glm/glm.hpp>

namespace NT
{
	class OpenGLRendererAPI : public RendererAPI
	{
	public:
		virtual void Init() override;

		virtual void SetClearColor(const glm::vec4& color) override;
		virtual void Clear() const override;

		virtual void DrawIndexed(const Ref<VertexArray>& vertexArray) override;
	};
}