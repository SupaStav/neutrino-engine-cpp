#pragma once

#include <iostream>
#include <memory>
#include <utility>
#include <algorithm>
#include <functional>

#include <string>
#include <sstream>
#include <array>
#include <vector>
#include <unordered_map>
#include <unordered_set>

#include <corecrt_math_defines.h>

#include "Neutrino/Core/Log.h"

#ifdef NT_PLATFORM_WINDOWS
#include <Windows.h>
#endif // NT_PLATFORM_WINDOWS
