#include <Neutrino.h>

#include <glm/gtc/matrix_transform.hpp>

#include "Platform/OpenGL/OpenGLShader.h"
//Example Layer
class ExampleLayer : public NT::Layer
{

private:
	float t = 0.0f;
	float e = 0.0f;
	float fps;
	NT::Window& m_Window;
	bool count = false;
public:

	ExampleLayer(NT::Window& window)
		: Layer("Example"), m_Camera(1.15f, 16.0f / 9.0f, 0.01f, 1000.0f), m_CamPos(0.0f), m_Window(window)
	{

		float vertices[3 * 7] = {
			-0.5f, -0.5f, 0.0f,/*color*/ 0.8f, 0.2f, 0.8f, 1.0f,
			 0.5f, -0.5f, 0.0f,/*color*/ 0.2f, 0.3f, 0.8f, 1.0f,
			 0.0f,  0.5f, 0.0f,/*color*/ 0.8f, 0.8f, 0.2f, 1.0f
		};

		NT::Ref<NT::VertexBuffer> TriangleVB;
		TriangleVB.reset(NT::VertexBuffer::Create(vertices, sizeof(vertices)));

		NT::BufferLayout layout = {
			{ NT::ShaderDataType::Float3, "a_Position" },
			{ NT::ShaderDataType::Float4, "a_Color" }
		};

		TriangleVB->SetLayout(layout);
		m_VertexArray->AddVertexBuffer(TriangleVB);

		NT::Ref<NT::IndexBuffer> TriangleIB;
		uint32_t indices[3] = { 0,1,2 };
		TriangleIB.reset(NT::IndexBuffer::Create(indices, sizeof(indices) / sizeof(uint32_t)));
		m_VertexArray->SetIndexBuffer(TriangleIB);

		m_SquareVertexArray.reset(NT::VertexArray::Create());

		float sqvertices[5 * 24] = {
			//front
			-0.5f, -0.5f, -1.0f, 0.0f, 0.0f,
			 0.5f, -0.5f, -1.0f, 1.0f, 0.0f,
			 0.5f,  0.5f, -1.0f, 1.0f, 1.0f,
			-0.5f,  0.5f, -1.0f, 0.0f, 1.0f,
			//back 4
			-0.5f, -0.5f, -2.0f, 0.0f, 0.0f,
			 0.5f, -0.5f, -2.0f, 1.0f, 0.0f,
			 0.5f,  0.5f, -2.0f, 1.0f, 1.0f,
			-0.5f,  0.5f, -2.0f, 0.0f, 1.0f,
			//left 8
			-0.5f, -0.5f, -2.0f, 0.0f, 0.0f,
			-0.5f,  0.5f, -2.0f, 0.0f, 1.0f,
			-0.5f, -0.5f, -1.0f, 1.0f, 0.0f,
			-0.5f,  0.5f, -1.0f, 1.0f, 1.0f,
			//right 12
			 0.5f,  0.5f, -2.0f, 1.0f, 1.0f,
			 0.5f, -0.5f, -2.0f, 1.0f, 0.0f,
			 0.5f,  0.5f, -1.0f, 0.0f, 1.0f,
			 0.5f, -0.5f, -1.0f, 0.0f, 0.0f,
			//top 16
			-0.5f,  0.5f, -2.0f, 0.0f, 0.0f,
			 0.5f,	0.5f, -2.0f, 0.0f, 1.0f,
			-0.5f,  0.5f, -1.0f, 1.0f, 0.0f,
			 0.5f,  0.5f, -1.0f, 1.0f, 1.0f,
			//bottom 20
			 0.5f, -0.5f, -2.0f, 0.0f, 0.0f,
			-0.5f, -0.5f, -2.0f, 0.0f, 1.0f,
			 0.5f, -0.5f, -1.0f, 1.0f, 0.0f,
			-0.5f, -0.5f, -1.0f, 1.0f, 1.0f
		};

		NT::Ref<NT::VertexBuffer> SquareVB;
		SquareVB.reset(NT::VertexBuffer::Create(sqvertices, sizeof(sqvertices)));

		SquareVB->SetLayout({
			{ NT::ShaderDataType::Float3, "a_Position" },
			{ NT::ShaderDataType::Float2, "a_TexCoord" }
			});
		m_SquareVertexArray->AddVertexBuffer(SquareVB);

		uint32_t sqindices[36] = { 0,1,2,2,3,0, 4,5,6,6,7,4, 8,9,10,10,9,11, 12,13,14,14,13,15, 16,17,18,18,17,19, 20,21,22,22,21,23};
		NT::Ref<NT::IndexBuffer> SquareIB;
		SquareIB.reset(NT::IndexBuffer::Create(sqindices, sizeof(sqindices) / sizeof(uint32_t)));
		m_SquareVertexArray->SetIndexBuffer(SquareIB);

		auto texShader = m_ShaderLibrary.Load("assets/shaders/texture.glsl");

		m_Texture = NT::Texture2D::Create("assets/textures/goodimage.png");

		std::dynamic_pointer_cast<NT::OpenGLShader> (texShader)->Bind();
		std::dynamic_pointer_cast<NT::OpenGLShader> (texShader)->UploadUniformInt("u_Texture", 0);
	}

	void OnUpdate(NT::Timestep ts) override
	{
		float DeltaTime = ts;

		if (NT::Input::IsKeyPressed(NT_KEY_W))
			m_CamPos += m_Camera.GetTransform().GetForward() * 2.0f * DeltaTime;
		if (NT::Input::IsKeyPressed(NT_KEY_S))
			m_CamPos += m_Camera.GetTransform().GetBackward() * 2.0f * DeltaTime;

		if (NT::Input::IsKeyPressed(NT_KEY_A))
			m_CamPos += m_Camera.GetTransform().GetLeft() * 2.0f * DeltaTime;
		if (NT::Input::IsKeyPressed(NT_KEY_D))
			m_CamPos += m_Camera.GetTransform().GetRight() * 2.0f * DeltaTime;

		if (NT::Input::IsKeyPressed(NT_KEY_SPACE))
			m_CamPos.y += 1.0 * DeltaTime;
		if (NT::Input::IsKeyPressed(NT_KEY_LEFT_SHIFT))
			m_CamPos.y -= 1.0 * DeltaTime;
		
		m_CamRot.x = NT::Input::GetMouseY() / -5;
		m_CamRot.y = NT::Input::GetMouseX() / -5;

		NT::RenderCommand::SetClearColor({ 0.2,0.2,0.2,1.0 });
		NT::RenderCommand::Clear();

		m_Camera.GetTransform().pos = m_CamPos;
		m_Camera.GetTransform().rot = glm::rotate(glm::mat4(1.0f), glm::radians(m_CamRot.z), glm::vec3(0, 0, 1)) * glm::rotate(glm::mat4(1.0f), glm::radians(m_CamRot.y), glm::vec3(0, 1, 0)) * glm::rotate(glm::mat4(1.0f), glm::radians(m_CamRot.x), glm::vec3(1,0,0));

		NT::Renderer::BeginScene(m_Camera);

		glm::mat4 SQScale = glm::scale(glm::mat4(1.0f), glm::vec3(0.1f));

		t += DeltaTime;
		e += DeltaTime;
		e /= 2.0f;
		fps = 1.0f / e;

		NT_ERROR(round(fps));

		auto textShader = m_ShaderLibrary.Get("texture");

		for (int y = 0; y < 19; y++)
		{
			for (int x = 0; x < 19; x++)
			{
				glm::vec3 pos(x * 0.11f,y * 0.11f, 0.0f);
				glm::mat4 SQTrans = glm::translate(glm::mat4(1.0f), pos) * SQScale;
				NT::Renderer::Submit(textShader, m_SquareVertexArray, SQTrans);
			}
		}

		m_Texture->Bind();
		NT::Renderer::Submit(textShader, m_SquareVertexArray, glm::scale(glm::mat4(1.0f), glm::vec3(1.0f)));

		//NT::Renderer::Submit(m_Shader, m_VertexArray);

		NT::Renderer::EndScene();
	}

	//use for typing text and UI
	void OnEvent(NT::Event& event) override
	{
	}

private:
	NT::ShaderLibrary m_ShaderLibrary;

	NT::Ref<NT::Shader> m_Shader;
	NT::Ref<NT::VertexArray> m_VertexArray;

	NT::Ref<NT::VertexArray> m_SquareVertexArray;

	NT::Ref<NT::Texture2D> m_Texture, m_Stuff;

	NT::PerspecCamera m_Camera;
	glm::vec3 m_CamPos;

	glm::vec3 m_SQColor = { 1.0f, 0.0f, 0.0f };

	float CamSpeed = 5.0f;
	glm::vec3 m_CamRot = { 0.0f, 0.0f, 0.0f };
};

class mcdonald : public NT::Entity
{
};

class Sandbox : public NT::Application
{
public:
	Sandbox()
	{
		GetWindow().SetMouseVisible(false);
		GetWindow().SetRawInput(true);
		GetWindow().SetVSync(false);
		PushLayer(new ExampleLayer(GetWindow()));
	}

	~Sandbox()
	{

	}
};
//Returns a new Sandbox app
NT::Application* NT::CreateApplication()
{
	return new Sandbox();
}